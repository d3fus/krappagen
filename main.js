var settings = {
  chat: {
    commands: ["!krappagen", "!k"],
    cooldown: 0,
    userCooldown: 1,
  },
  emoteSettings: {
    gif: false,
    blacklist: [],
    twitch: false,
    twitchGlobal: false,
    sevenTv: false,
    sevenTvGlobal: false,
    bttv: false,
    bttvGlobal: false,
    ffz: false,
    ffzGlobal: false
  },
  animation: "explosion",
  explosion: {
    limit: 400,
    default: 200,
    duration: 2,
  },
  firework: {
    limit: 400,
    default: 200,
    duration: 2
  },
  fountain: {
    limit: 400,
    default: 200,
    duration: 8
  }
};
var channel = {
  apiKey: "",
  id: ""
};
var allEmotes = {
  twitch:[],
  twitchGlobal:[],
  sevenTv:[],
  sevenTvGlobal:[],
  bttv:[],
  bttvGlobal: [],
  ffz:[],
  ffzGlobal: []
};
var animations = {
  explosion: null,
  firework: null,
  fountain: null
};
var enabledEmotes = [];

var cooldown = Math.floor(Date.now() / 1000);
var userCooldown = {};
var emote = [];
var http = new XMLHttpRequest();

window.addEventListener('onWidgetLoad', function (obj) {
  channel.apiKey = obj.detail.channel.apiToken;
  channel.id = obj.detail.channel.id;
  var fieldData = obj.detail.fieldData;

  donwloadAllEmotesStreamelements();

  settings.emoteSettings.twitch = fieldData.emoteProviderTwitchChannel;
  settings.emoteSettings.twitchGlobal = fieldData.emoteProviderTwitchGlobal;
  settings.emoteSettings.sevenTv = fieldData.emoteProviderSevenTvChannel;
  settings.emoteSettings.sevenTvGlobal = fieldData.emoteProviderSevenTvGlobal;
  settings.emoteSettings.bttv = fieldData.emoteProviderBttvChannel;
  settings.emoteSettings.bttvGlobal = fieldData.emoteProviderBttvGlobal;
  settings.emoteSettings.ffz = fieldData.emoteProviderFfzChannel;
  settings.emoteSettings.ffzGlobal = fieldData.emoteProviderFfzGlobal;
  settings.emoteSettings.gif = fieldData.emoteEnableGif;
  settings.emoteSettings.blacklist = fieldData.emoteBlackList.replace(/\s/g, "").split("|");

  settings.animation = fieldData.commandOptions;

  settings.chat.cooldown = fieldData.chatCommandCoolDown;
  settings.chat.userCooldown = fieldData.chatCommandUserCoolDown;
  settings.chat.commands = fieldData.chatCommands.replace(/\s/g, "").split("|");

  settings.explosion.duration = fieldData.explosionAnimationLength;
  settings.explosion.default = fieldData.explosionDefault;
  settings.explosion.limit = fieldData.explosionLimit;

  settings.firework.duration = fieldData.fireworkAnimationLength;
  settings.firework.default = fieldData.fireworkDefault;
  settings.firework.limit = fieldData.fireworkLimit;

  settings.fountain.duration = fieldData.fountainAnimationLength;
  settings.fountain.default = fieldData.fountainDefault;
  settings.fountain.limit = fieldData.fountainLimit;

  //console.log(settings);
});

window.addEventListener('onEventReceived', function (obj) {
  if (obj.detail.listener == "message") {
    var event = obj.detail.event;
    var msg = event.data.text;

    var ts = Math.floor(Date.now() / 1000);

    if (settings.chat.commands.map(x => msg.startsWith(x)).includes(true) && ts - cooldown >= settings.chat.cooldown) {
      var user = event.nick;
      if (userCooldown[user] != null) {
        var userTs = userCooldown[user];
        if (ts - userTs < settings.chat.userCooldown)
          return;
      }

      var tmpEmotes = enabledEmotes;
      var num = null;
      if (event.data.emotes.length > 0) {
        var emotes = event.data.emotes;
        num = msg.split(" ");
        num = parseInt(num[num.length - 1]);
        if (num != "NaN") {
          num = settings.explosion.default;
          num = Math.min(Math.max(num, 1), settings.explosion.limit);
        }

        tmpEmotes = parseEmotes(emotes, []);
      }

      switch (settings.animation) {
        case "explosion":
          animations.explosion(num == null ? settings.explosion.default : num, tmpEmotes);
          break;
        case "firefork":
          animations.firework(num == null ? settings.firework.default : num, tmpEmotes);
          break;
        case "fountain":
          animations.fountain(num == null ? settings.fountain.default : num, tmpEmotes);
          break;
        case "random":
          var animation = Object.keys(animations);
          var animation = animation[Math.floor(Math.random() * animation.length)];
          animations[animation](num == null ? settings[animation].default : num, tmpEmotes);
          break;
        default:
          break;
      }

      if (settings.chat.cooldown > 0) {
        cooldown = ts;
      }
      if (settings.chat.userCooldown > 0) {
        userCooldown[user] = ts;
      }
    }
  }
});
console.log("krappagen ready");

//NOTE testing stuff
http.onreadystatechange = function() {
  if (http.readyState === 4) {
    emote = [];
    //TODO add regex
    // and better parsing
    switch (http.responseURL.split("/")[2]) {
      case "api.streamelements.com":
        if (http.responseURL.includes("emotes")) {
          parseStreamElementsEmotes(http.response);
        } else {
          //parseStreamElementsUserPoints(http.response);
        }
        break;
      case "twitch.tv":
        break;
      case "api.7tv.app":
        emote = JSON.parse(http.response);
        break;
      default:
        console.log(http.responseURL)
    }
  }
}

function donwloadAllEmotesStreamelements() {
  http.open("GET", "https://api.streamelements.com/kappa/v2/channels/" + channel.id + "/emotes");
  http.setRequestHeader("Accept", "application/json");
  http.setRequestHeader("Authorization", "Bearer " + channel.apiKey);
  http.send();
}

function parseStreamElementsEmotes(resp) {
  var emotes = JSON.parse(resp);
  allEmotes.twitch = Object.values(emotes.twitchSubEmotes);
  allEmotes.twitchGlobal = Object.values(emotes.twitchGlobalEmotes);
  allEmotes.sevenTv = Object.values(emotes.sevenTVChannelEmotes);
  allEmotes.sevenTvGlobal = Object.values(emotes.sevenTVGlobalEmotes);
  allEmotes.bttv = Object.values(emotes.bttvChannelEmotes);
  allEmotes.bttvGlobal = Object.values(emotes.bttvGlobalEmotes);
  allEmotes.ffz = Object.values(emotes.ffzChannelEmotes);
  allEmotes.ffzGlobal = Object.values(emotes.ffzGlobalEmotes);
  //console.log(allEmotes);
  updateEmotes();
}

function updateEmotes() {
  enabledEmotes = [];
  var keys = Object.keys(allEmotes);
  for(i = 0; i < keys.length; i++) {
    if (settings.emoteSettings[keys[i]]) {
      enabledEmotes = parseEmotes(allEmotes[keys[i]], enabledEmotes);
    }
  }
}

function parseEmotes(emotes, enabledEmotes) {
  for(i = 0; i < emotes.length; i++) {
    var e = emotes[i];
    //TODO filter
    // NOTE streamelements is not tagging the emotes right, needs a workaround to filter gifs
    if (!settings.emoteSettings.blacklist.includes(e.name) && (settings.emoteSettings.gif == e.gif || e.gif == false)) {
      var urls = Object.values(e.urls);
      var url = urls[urls.length -1];
      var tmpImg = new Image();
      tmpImg.src = url;
      enabledEmotes.push(url);
    }
  }
  return enabledEmotes;
}

animations.explosion = function(amount, enabledEmotes) {
    var seed = Math.floor(Math.random() * enabledEmotes.length);

    var startx = Math.random() * 70;
    var starty = Math.random() * 70;
    for(i = 0; i < amount; ++i) {
      setTimeout(function(k) {
        var e = enabledEmotes[(k+seed) % enabledEmotes.length];
        var imgPath = e;
        var img = document.createElement("img");
        img.src = imgPath;

        img.style.position = "absolute";
        img.style.top = startx + "%";
        img.style.left = starty + "%";
        img.style.height = "0rem";

        var vx = Math.random() - 0.5;
        var vy = Math.random() - 0.5;
        var v = 1 / Math.sqrt(vx * vx + vy * vy);
        vx *= v;
        vy *= v;
        var animation = [
          {height: 6 + 2 * Math.random() + "rem"},
          {top: (50 + 130 * vy) + "%", left: (50 + 130 * vx) + "%", height: 6 + 2 * Math.random() + "rem"}
        ];
        var timing = {delay: settings.explosion.duration * 1000 * Math.random() , duration: 8000, easing: "cubic-bezier(0.215, 0.61, 0.355, 1)"};

        img.animate(animation, timing).onfinish = function(e){img.remove()};
        document.body.appendChild(img);

        }, 5*i, i);
    }
}

animations.firework = function(amount, enabledEmotes) {
  var seed = Math.floor(Math.random() * enabledEmotes.length);
  var startx = Math.random() * 50 + 25;
  var starty = Math.random() * 50 + 10;


  var sparks = [];

  for(i = 1; i < amount; ++i) {
    var emote = enabledEmotes[(i+seed) % enabledEmotes.length];
    var imgPath = emote;
    var img = document.createElement("img");
    img.src = imgPath;

    img.style.position = "absolute";
    img.style.left = startx + "%";
    img.style.top = starty + "%";
    img.style.height = "7rem";
    img.style.display = "none";
    document.body.appendChild(img);
    sparks.push(img);
  }

  var startemote = enabledEmotes[seed];
  var img = document.createElement("img");
  img.src = startemote;
  img.style.position = "absolute";
  img.style.left = "50%";
  img.style.top = "100%";
  img.style.height = "7rem";
  document.body.appendChild(img);

  var animation = [
    {top: starty+"%", left: startx+"%"}
  ];
  var timing = {duration: 200 * (settings.firework.duration + 1), easing: "linear"};

  img.animate(animation, timing).onfinish = function(e){
    img.remove();
    for(var i=0;i<sparks.length;++i) {
      setTimeout(function(k) {
        var v = Math.random();
        var vx = Math.random() - 0.5;
        var vy = Math.random() - 0.5;
        var nv = v/Math.sqrt(vx * vx + vy * vy);
        vx *= nv;
        vy *= nv;

        var img = sparks[k];
        img.style.display = "block";

        var animation = [
          {top: (starty + 100 * vy) + "%", left: (startx + 100 * vx) + "%", opacity: 0, height: 0}
        ];
        var timing = {duration: 800 * (settings.firework.duration + 1), easing: "cubic-bezier(0.215, 0.61, 0.355, 1)"}
        img.animate(animation, timing).onfinish = function(e){img.remove()};
      }, 1, i);
    }
  };
}

animations.fountain = function(amount, enabledEmotes) {
  var seed = Math.floor(Math.random() * enabledEmotes.length);
  var startx = 50;
  var starty = 100;
  var fountainEmotes = [];
  for(i = 0; i < amount; ++i) {
    setTimeout(function(k) {
    var emote = enabledEmotes[(k + seed) % enabledEmotes.length];
    var img = document.createElement("img");
    img.src = emote;
    img.style.position = "absolute";
    img.style.left = startx + "%";
    img.style.top = starty + "%";
    img.style.height = "0rem";
    document.body.appendChild(img);

    var animation = [
      {height: "7rem"}
    ];
    var timing = {duration: 1000 * settings.fountain.duration, easing: "cubic-bezier(0.215, 0.61, 0.355, 1)"};
    img.animate(animation, timing);

    var vx = (Math.random() - 0.5) * 2;
    var vy = (Math.random() + 1.5 );
    var nv = (Math.random() + 1) / (2 * Math.sqrt(vx * vx + vy * vy) * (settings.fountain.duration + 1));
    vx *= nv * 100;
    vy *= nv * 300;
    fountainEmotes.push({elem: img, x: startx, y: starty, vx: vx, vy: vy, v0: vy});
    }, 10 * settings.fountain.duration * i * 7 / 112, i);

  }
  var rafStart = Date.now();
  var wait = true;
  var positionChanger = function(t) {
    var f = (t - rafStart) / 1000.0;
    var speedmodifier = 1 / (settings.fountain.duration + 1);
    var speedmodifiersq = speedmodifier * speedmodifier;
    for(var i=fountainEmotes.length - 1; i >= 0; --i) {
      var emote = fountainEmotes[i];
      emote.x += emote.vx * f;
      emote.vy -= 600 * speedmodifiersq * f;
      emote.y -= emote.vy * f;
      if(emote.y > 120) {
        emote.elem.remove();
        fountainEmotes.splice(i, 1);
        if (fountainEmotes.length == 0) {
          wait = false;
        }
      } else {
        emote.elem.style.left = emote.x + "%";
        emote.elem.style.top = emote.y + "%";
      }
    }
    if (wait) {
      rafStart = t;
      window.requestAnimationFrame(positionChanger);
    }
  }
  window.requestAnimationFrame(positionChanger);
}
